
class Code
  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }

  attr_reader :pegs
  def initialize(pegs)
    @pegs = pegs
  end

  def [](idx)
    pegs[idx]
  end

  def self.parse(input)
    pegs = input.split("").map do |letter|
      raise "incorrect input" unless PEGS.has_key?(letter.upcase)
      PEGS[letter.upcase]
    end
    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample }
    Code.new(pegs)
  end

 def exact_matches(guess)
    count = 0
    4.times {|idx| count += 1 if self[idx] == guess.pegs[idx]}
    count
 end

 def near_matches(guess)
   count = 0
   pegs_copy = self.pegs.select { |peg| peg }
   exact_count = self.exact_matches(guess)
   guess.pegs.each do |peg|
     if pegs_copy.include?(peg)
      count += 1
      pegs_copy.delete(peg)
     end
   end
   count - exact_count
 end

  def ==(guess)
    return false unless guess.is_a?(Code)
    self.pegs == guess.pegs
  end

end

class Game
  MAX_TURNS = 10

  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def play
    MAX_TURNS.times do
      guess = get_guess
      if guess == @secret_code
        puts "You got it!"
        return
      end
      display_matches(guess)
    end
    puts "Ups sorry you missed it"
  end

  def get_guess
    puts "Guess the code:"

    begin
      Code.parse(gets.chomp)
    rescue
      puts "incorrect input!"
      retry
    end
  end

  def display_matches(guess)
    exact_matches = @secret_code.exact_matches(guess)
    near_matches = @secret_code.near_matches(guess)
    puts "You got #{exact_matches} exact matches!"
    puts "You got #{near_matches} near matches!"
  end
end
